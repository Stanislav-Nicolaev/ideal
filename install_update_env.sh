#!/bin/bash
# Execute this file with the following command:
# ./Install_update_env.sh

################ IMPORTANT!! ################
# python3 must be your default Python version!  
# Recomended version is python 3.9.7.

# Note: Please select the python iterpreter inside environment to development, also in IDE's (VSCode, PyCharm, Spyder, etc). 
# Interpreter path: /env/bin/python3
##

# Update pip
python3.9 -m pip install --user --upgrade pip

# Install venv
python3.9 -m pip install --user virtualenv

# Create ideal virtual environment called 'env'
python3.9 -m virtualenv env

# Activate environment
source env/bin/activate

# Automaticaly install dependencies
pip install -r requirements_ideal.txt

# Deactive environment after running
deactivate

