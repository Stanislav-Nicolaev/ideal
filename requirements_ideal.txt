aiohttp==3.8.1
aiohttp-socks==0.7.1
aiorpcX==0.18.7
aiosignal==1.2.0
async-timeout==4.0.2
attrs==21.4.0
bitstring==3.1.9
certifi==2021.10.8
charset-normalizer==2.0.12
cryptography==36.0.1
dnspython==2.2.1
frozenlist==1.3.0
idna==3.3
multidict==6.0.2
protobuf==3.19.4
pycryptodomex==3.14.1
PyQt5==5.15.6
PyQt5-Qt5==5.15.2
PyQt5-sip==12.9.1
python-socks==2.0.3
qrcode==7.3.1
typing_extensions==4.1.1
yarl==1.7.2
