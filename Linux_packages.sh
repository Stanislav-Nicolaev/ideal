#!/bin/bash

# Default python version of project.
pythonversion='3.9.7'

# List of needed Linux packages
pkgs='libsecp256k1-dev libxcb-xinerama0'

# Flag to check if any package needs to be installed.
install=false

# Checking if needed Linux packages are installed.
for pkg in $pkgs; do
  # Status of the package
  status="$(dpkg-query -W --showformat='${db:Status-Status}' "$pkg" 2>&1)"
  # If not installed asks for insallation
  if [ ! $? = 0 ] || [ ! "$status" = installed ]; then
    install=true
    echo "Package $pkg not installed"
    echo "Do you wish to install the package $pkg (y/n)?"
    
    # User needs to agree to instalation pressing 'y' on keyboard
    old_stty_cfg=$(stty -g)
    stty raw -echo
    answer=$( while ! head -c 1 | grep -i '[ny]' ;do true ;done )
    stty $old_stty_cfg
    
    # If user agreeds, install the needed package. Else skip the installation of this package.
    if echo "$answer" | grep -iq "^y" ;then
        echo "Installing"
        sudo apt install $pkg
        install=false
    else
        echo "Skipping"
    fi
  fi
done

# Checking if some package was not installed.
if $install; then
  echo "If any of needed linux packages is missing the application could not work."
else
  echo "All needed linux packages are installed."
fi

# Python 3 version in Operational System
version=$(python3 -V 2>&1 | grep -Po '(?<=Python )(.+)')

# Testing if python3 version is valid
if [[ "$version" != "$pythonversion"|| -z "$version" ]]
then
  echo "Invalid python3 version, it should be $pythonversion but is $version"
  echo "Do you want Install Python $pythonversion and set as default python3 (y/n)?"
  # User needs to agree to instalation pressing 'y' on keyboard
  old_stty_cfg=$(stty -g)
  stty raw -echo
  answer=$( while ! head -c 1 | grep -i '[ny]' ;do true ;done )
  stty $old_stty_cfg
  
  # If user agreeds, install. Else skip the installation.
  if echo "$answer" | grep -iq "^y" ;then
      echo "Installing Python $pythonversion trough file Python3_installation.sh"
      # Run the shell script to install python3.
      source ./Python3_installation.sh
  else
      echo "Skipping"
  fi
    
else
  echo "The python3 version is valid, the version is: $version"
fi
