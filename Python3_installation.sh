#!/bin/bash
# Bash script to intall some python version from source code

# Python version to be installed
pythonversion="3.9.7"
echo "Installing Python $pythonversion"

# Apt update
sudo apt update

# Install the dependencies necessary to build Python
sudo apt install build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev libsqlite3-dev wget libbz2-dev

# Download the python3 release source code from the Python download page with wget
wget https://www.python.org/ftp/python/$pythonversion/Python-$pythonversion.tgz

# Once the download is complete, extract the gzipped archive
zipfile="Python-$pythonversion.tgz" 
tar -xf $zipfile

# Run the configure script in Python folder, 
## which performs a number of checks to make sure all of the dependencies on your system are present
cd Python-$pythonversion/
./configure --enable-optimizations

# Start the Python 3.9 build process
make -j 12

# Install the Python binaries, and set and overwrite
sudo make install

# Set python3.9 as default python3
alias python3=python3.9

# Removing downloaded files
cd ..
sudo rm -rf Python-$pythonversion/
sudo rm $zipfile