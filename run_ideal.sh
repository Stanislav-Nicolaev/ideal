#!/bin/bash

# Testing if Linux packages and python3 version.
./Linux_packages.sh

# Run ideal
if [ -f "./env/bin/activate" ]; then
    
    echo "The python3 dependencies are installed, if you need update then please run './install_update_env.sh'"
    # Running application from env
    ./env/bin/python run_electrum
    
    # If this is an old directory and our requirements
    #       changed in the meantime you should run "./install_update_env.sh"
else
    echo "The python3 dependencies are not Installed yet, running './install_update_env.sh' first."
    
    # Installing dependencies
    ./install_update_env.sh
     
    # Running application from env
    ./env/bin/python run_electrum
fi
