#!/usr/bin/env python
#
# Electrum - lightweight Bitcoin client
# Copyright (C) 2015 Thomas Voegtlin
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys
import string
import asyncio
from typing import Any, Callable

from PyQt5.QtCore import Qt, QEvent, QPoint
from PyQt5.QtGui import QPixmap, QStandardItemModel, QStandardItem
from PyQt5.QtWidgets import (QAbstractItemView, QApplication, QDialog,
                             QHBoxLayout, QLabel, QMainWindow, QMenu)

from electrum.i18n import _
from electrum.util import format_time, OrderedDictWithIndex
from electrum import transaction
from electrum.bitcoin import *
from electrum.crypto import sha256
from .util import MyTreeView, read_QIcon, icon_path, MySortModel, pr_icons, QModelIndex

if TYPE_CHECKING:
    from .main_window import ElectrumWindow


# Disabling the python cache creation found as __pycache__ files in the repository,
# this may affect python performance but don't mistake with the performance of network requisitions
sys.dont_write_bytecode = True

ROLE_REQUEST_TYPE = Qt.UserRole
ROLE_REQUEST_ID = Qt.UserRole + 1
ROLE_SORT_ORDER = Qt.UserRole + 2


class IdealTransaction:
    # Hex of capital letter "P"
    PRICING = '50'
    # Hex of capital letter "T"
    TRANSFER = '54'
    # The length of appropriation OP_Return message in Ideal standard
    APPROPRIATION_LENGTH = 34
    # The length of bytes used by op codes
    OP_CODE_LEN = 1
    # Size of op code and message length
    OP_MESSAGE = 2
    # Standard Op_Return message for transferring identifier
    TRANSFER_SCRIPT = [106] + [1] + [int(TRANSFER, 16)]
    # Standard Op_Return message for pricing identifier
    PRICING_SCRIPT = [106] + [1] + [int(PRICING, 16)]
    # Identifier operations
    IDENTIFIER_OPERATIONS = [bytes(TRANSFER_SCRIPT), bytes(PRICING_SCRIPT)]
    # Standard encoding
    ENCODING = 'UTF-8'
    # Types of addresses implemented in Electrum Wallet
    implemented_addr_types = ['p2pkh', 'p2wpkh', 'p2wpkh-p2sh']

    def __init__(self, parent: 'ElectrumWindow'):
        self.parent = parent
        self.appropriated_address = ''
        self._transactions = OrderedDictWithIndex()

    @property
    def transactions(self):
        return {}

    def is_op_return(self, op_code_script: bytes) -> bool:
        """
            Tells whether the op code script is an op return with treat of script size

        Parameters
        ----------
        op_code_script : bytes
            The op code script to be evaluated

        Returns
        -------
        bool
            The result of the test as op return script
        """
        if len(op_code_script) >= self.OP_MESSAGE:
            return int.from_bytes(op_code_script[0:1], 'big') == int(opcodes.OP_RETURN)
        else:
            return False

    def ask_txids(self, addresses: list[str]):
        """
            Asks Network for the transactions of the addresses

        Parameters
        ----------
        addresses : list[str]
            The list of addresses derived from address_from_identifier

        Returns
        -------
        list[dict]
            The list of transactions ids and block heights
        """
        addresses_txids = []
        for address in addresses:
            if not is_address(address):
                continue
            try:
                scripthash = address_to_scripthash(address)
                address_txids = self.parent.network.run_from_another_thread(
                    self.parent.network.get_history_for_scripthash(scripthash))
                addresses_txids += address_txids
            except ValueError:
                pass
        return addresses_txids

    def load_transactions(self, tx_list: list[dict]):
        """
            Load transactions information from the Network

        Parameters
        ----------
        tx_list: list[dict]
            The transaction IDs to get the transactions

        Returns
        -------
        OrderedDictWithIndex
            Dictionary with the transactions and their respective information
        """
        transactions = OrderedDictWithIndex()
        for tx_dict in tx_list:
            txid = tx_dict['tx_hash']
            serialized_tx = self.parent.wallet.get_input_tx(txid)
            tx = self.extract_serialized_tx(serialized_tx, tx_dict['height'])
            if not tx:
                return None
            transactions.update(tx)
        return transactions

    def extract_serialized_tx(self, tx: transaction.Transaction,
                              height: int) -> Optional[OrderedDictWithIndex]:
        """
            Deserializes the transaction

        Parameters
        ----------
        tx : transaction.Transaction
            Serialized transaction
        height : int
            Height of the block in which the transaction was mined

        Returns
        -------
        OrderedDictWithIndex
            Information of the transaction organized in a dictionary,
            returns None if network gets error and tx is not a Transaction type
        """
        deserialized_tx = OrderedDictWithIndex()
        # Treat case network gets error
        if not isinstance(tx, transaction.Transaction):
            return None
        deserialized_tx.update({tx.txid(): {
            'txid': tx.txid(),
            'height': height,
            'input': tx.inputs(),
            'output': tx.outputs(),
            'serialized_tx': tx
        }})
        return deserialized_tx

    def filter_op_returns(self, transactions: Union[dict, OrderedDictWithIndex]) -> OrderedDictWithIndex:
        """
            Filter the transactions with OP_Returns

        Parameters
        ----------
        transactions : Union[dict, OrderedDictWithIndex]
            The transactions to be verified with OP_Returns to be returned

        Returns
        -------
        OrderedDictWithIndex
            Returns only the transactions found with OP_Returns
        """
        op_return_transactions = OrderedDictWithIndex()
        if not transactions:
            return op_return_transactions
        for tx_dict in transactions.values():
            txid = tx_dict['txid']
            serialized_tx = self.parent.wallet.get_input_tx(txid)
            tx = self.extract_serialized_tx(serialized_tx, tx_dict['height'])
            if not tx:
                continue
            for each_output in tx[txid]['output']:
                if self.is_op_return(each_output.scriptpubkey):
                    # Only transactions with OP_Return matters
                    op_return_transactions.update(tx)
                    break
        return op_return_transactions

    def get_input_address(self, tx_id: str, transactions: dict = None) -> list[str]:
        """
            Find the addresses in the input of a transaction

        Parameters
        ----------
        tx_id : str
            id of transaction
        transactions : dict
            Transactions information which contains inputs and outputs addresses

        Returns
        -------
        list[str]
            The list of addresses in the input of the transaction
        """
        serialized_tx = transactions[tx_id]['serialized_tx'] \
            if transactions is not None else self.transactions[tx_id]['serialized_tx']
        inputs_list = serialized_tx.inputs()
        input_addresses = []
        for each_input in inputs_list:
            previous_hash = each_input.prevout.txid.hex()
            previous_serialized_tx = self.parent.wallet.get_input_tx(previous_hash)
            input_addresses.append(previous_serialized_tx.outputs()[each_input.prevout.out_idx].address)
        # Returns with the duplicate removed with list(dict.fromkeys(x))
        return list(dict.fromkeys(input_addresses))

    def get_op_return_script(self, tx_with_op_return: Union[dict, OrderedDictWithIndex]) -> bytes:
        """
            Get the script of OP_Return in the output of an identifier operation transaction

        Parameters
        ----------
        tx_with_op_return : Union[dict, OrderedDictWithIndex]
            The identifier operation transaction with OP_Return

        Returns
        -------
        bytes
            The script of OP_Return
        """
        script = [each_output.scriptpubkey
                  for each_output in tx_with_op_return['output']
                  if self.is_op_return(each_output.scriptpubkey)][0]
        return script

    def get_output_addresses(self, tx_outputs: list[transaction.TxOutput]) -> list[str]:
        """
            Get the addresses in the output of a transaction

        Parameters
        ----------
        tx_outputs : list[transaction.TxOutput]
            The list of outputs

        Returns
        -------
        list[str]
            The addresses in the output of a transaction
        """
        addresses = [each_output.address for each_output in tx_outputs if each_output.address is not None]
        return addresses

    def check_id_operation(self):
        pass

    def set_identifier_ownership(self, has_transferring: bool, operation_txid: str,
                                 appropriated_address: str, owner_address: str):
        pass

    def check_condition_script(self, operation_tx: dict, is_appropriation: bool, id_hash: bytes = None):
        """
            Check the conditions of the Op_Return script

        Parameters
        ----------
        operation_tx : dict
            The dictionary with the identifier operation transaction
        is_appropriation : bool
            Whether it must verify if is appropriation or transferring
        id_hash : bytes
            The hash of the identifier

        Returns
        -------
        bool
            Whether it has one of the identifier operations
        bool
            Whether it has the transferring identifier operation
        bytes
            The OP_Return script of the transaction
        """
        op_return_script = self.get_op_return_script(operation_tx)
        # Check with id_hash when this is given, else check the size of standard appropriation or identifier operations
        has_script_format = to_bytes(op_return_script[self.OP_MESSAGE:]) == to_bytes(id_hash) if id_hash \
            else len(op_return_script) == self.APPROPRIATION_LENGTH or len(op_return_script) == self.OP_MESSAGE+1
        has_transferring = to_bytes(op_return_script) == bytes(self.TRANSFER_SCRIPT)
        has_id_operation = has_script_format if not is_appropriation else has_transferring
        return has_id_operation, has_transferring, op_return_script

    def check_condition_output(self, output_addresses: list[str],
                               *,
                               improper_addresses: list[str] = None,
                               op_return_script: bytes = None,
                               owner_address: str = None):
        """
            Check the conditions of transaction output
            For an appropriation
                A single owner in input,
                a single improper address in output and
                an OP_Return with the identifier hash
            For a transferring
                A single owner in input,
                a single improper address in output,
                a single address as new owner in output,
                an Op_return with string operation

        Parameters
        ----------
        output_addresses : list[str]
            The addresses in the output of the transaction
        improper_addresses : list[str]
            The improper addresses of the identifier
        op_return_script : bytes
            The Op_Return script in the output of the transaction
        owner_address : str
            The address of the owner of the identifier

        Returns
        -------
        bool
            The evaluation that there is a single improper address in the output of the transaction
        str
            The improper address in the output of the transaction
        list[str]
            The improper addresses of the identifier
        """
        if not improper_addresses and op_return_script is not None:
            if op_return_script == bytes(self.TRANSFER_SCRIPT):
                improper_addresses = [output_addr for output_addr in output_addresses if output_addr != owner_address]
            else:
                improper_privkeys = [serialize_privkey(sha256(op_return_script[self.OP_MESSAGE:]), True, addr_type)
                                     for addr_type in self.implemented_addr_types]
                improper_addresses = [address_from_private_key(wif_key) for wif_key in improper_privkeys]
        # List intersection with list(set(x)&set(y))
        improper_address = list(set(output_addresses) & set(improper_addresses))
        has_improper_address = len(improper_address) == 1
        if has_improper_address:
            improper_address = improper_address[0]

        return has_improper_address, improper_address, improper_addresses

    def check_condition_transfer(self, has_transferring: bool,
                                 output_addresses: list[str],
                                 improper_addresses: list[str]) -> Tuple[bool, str]:
        """
            Check the conditions for an identifier transferring

        Parameters
        ----------
        has_transferring : bool
            Evaluation whether there is or not the script of identifier transferring
        output_addresses : list[str]
            The addresses in the output of the transaction
        improper_addresses : list[str]
            The improper addresses of the identifier

        Returns
        -------
        has_transferred_addr: bool
            Evaluation of the single address in the output of the transaction
        transferred_address: str
            The new owner address that the identifier is transferred to
        """
        if has_transferring:
            transferred_address = [each_addr for each_addr in output_addresses
                                   if each_addr not in improper_addresses]
            has_transferred_addr = len(transferred_address) == 1
            transferred_address = transferred_address[0] if has_transferred_addr else ''
        else:
            has_transferred_addr = False
            transferred_address = ''
        return has_transferred_addr, transferred_address

    def ask_block_header(self, height: int) -> Union[dict, None]:
        """
            Asks Network for the black header from given height

        Parameters
        ----------
        height: int
            The height of the block which the header is returned

        Returns
        -------
        Union[dict, None]
            The header of the block as dictionary, returns None if it fails
        """
        if self.parent.network.interface is None:
            return None
        header = None
        try:
            header = self.parent.network.run_from_another_thread(
                self.parent.network.interface.get_block_header(height, 'catchup'))
            # Print test with successor of genesis block
            # print(self.parent.network.run_from_another_thread(
            #     self.parent.network.interface.get_block_header(1, 'catchup')))
        except ValueError:
            pass
        return header


class Identifier(IdealTransaction):
    """
        Class to set all Identifier attributes and rules of Ideal standard
    """
    def __init__(self, parent: 'ElectrumWindow',
                 *, identifier: str = None, id_hash: Union[str, bytes] = '',
                 appropriated_address: str = '', owner: 'OwnerAddress' = None):
        """
            This constructor should be called with only one of those
                name 'identifier' or a hash of the identifier 'id_hash'
                or the address appropriated 'appropriated_address'
            When the name 'identifier' is given it ignores 'id_hash',
            When the 'appropriated_address' is given it must check against the improper addresses generated from
            the hash in the appropriation transaction
            If none is given it will do nothing

        Parameters
        ----------
        identifier : str
            The string naming the identifier, which is the identifier itself and will be used to calculate the hash
            and attributes
        id_hash : Union[str, bytes]
            The hash of the Identifier as string with 64 characters long or bytes with 32 bytes long which will be
            used to calculate the attributes
        appropriated_address: str
            The address appropriated of this identifier which must be checked against the
            hash of the OP_Return transaction
        owner: 'OwnerAddress'
            The possible owner of this identifier, given when it is evaluated before this identifier to save time
        """
        super().__init__(parent)
        self.parent = parent

        self._id_name = ""
        self._id_hash = ''
        self.is_named = False
        self.is_owned = False
        self.id_privkey = ''
        self.addr_key_pair = {}
        self.current_owner = owner
        self.appropriation_txid = ''
        self.current_operation_tx = OrderedDictWithIndex()
        self.improper_addr_txids = []
        self.historic_owner_addresses = []
        self.historic_operations_txids = []

        self.set_id(identifier)
        self._set_id_hash(id_hash)
        self._set_appropriated_address(appropriated_address)
        self.check_id_operation()

    def __str__(self):
        """
            Customize the string representation of this class instance

        Returns
        -------
        str
            The string representation of this class instance with the main information
            followed by the address appropriated, private key as WIF and owner address if is the case
        """
        header = ["ID", "Hash"]
        attributes = [self.id_name, self.id_hash.hex()]
        if self.is_owned:
            header += ["Address Appropriated", "WIF Private Key", "Current Owner Address"]
            attributes += [self.appropriated_address, self.id_privkey, self.current_owner.owner_address]
        string_representation = ", ".join([": ".join(each_pair) for each_pair in zip(header, attributes)])
        return string_representation

    @ property
    def improper_privkeys(self) -> list[str]:
        """
            Set property improper_privkeys from the id_hash attribute

        Returns
        -------
        list[str]
            The list of possible private keys set from id_hash and implemented types of electrum
        """
        privkeys = [serialize_privkey(sha256(self.id_hash), True, addr_type)
                    for addr_type in self.implemented_addr_types] if self.id_hash else []
        return privkeys

    @property
    def improper_addresses(self) -> list[str]:
        """
            Set the property improper_addresses generated from the property improper_privkeys

        Returns
        -------
        list[str]
            The list of possible improper addresses
        """
        [self.addr_key_pair.update({address_from_private_key(wif_key): wif_key})
         for wif_key in self.improper_privkeys]
        improper_addresses = list(self.addr_key_pair.keys())
        return improper_addresses

    @property
    def id_name(self) -> str:
        """
            Set property id_name with the name of this identifier

        Returns
        -------
        str
            The identifier name as string
        """
        return self._id_name

    @property
    def id_hash(self) -> bytes:
        """
            Set property identifier hash

        Returns
        -------
        bytes
            The hash of id_name
        """
        return to_bytes(self._id_hash)

    @property
    def transactions(self) -> OrderedDictWithIndex:
        """
            Set property transactions with OP_Returns related with this Identifier

        Returns
        -------
        OrderedDictWithIndex
            Returns the transactions filtered with OP_Returns
        """
        # This condition is applied to shorten the time of transactions by not updating every access to it
        if not self._transactions and self.parent.network.is_connected():
            txids = self.ask_txids(self.improper_addresses) \
                if self._id_hash else self.ask_txids([self.appropriated_address])
            transactions = self.load_transactions(txids)
            self._transactions = self.filter_op_returns(transactions)
        return self._transactions

    @property
    def appropriation_transaction(self) -> OrderedDictWithIndex:
        """
            Set property appropriation_transaction with the valid appropriation of this Identifier

        Returns
        -------
        OrderedDictWithIndex
            The dictionary with the appropriation transaction
        """
        if self.appropriation_txid in self.transactions.keys():
            return self.transactions[self.appropriation_txid]
        else:
            return OrderedDictWithIndex()

    @property
    def block_header(self) -> Union[dict, None]:
        """
            Set the property block_header containing the header of the block of the mined transaction

        Returns
        -------
        Union[dict, None]
            The dictionary with block header, None if it fails
        """
        if self.is_owned and self.parent.network.is_connected():
            height = self.current_operation_tx['height']
            return self.ask_block_header(height)

    def set_id(self, identifier: str):
        """
            Test if the identifier given is a string and format
            Set the attribute identifier string

        Parameters
        ----------
        identifier : str
            The identifier string to be set
        """
        if self.is_named or identifier is None:
            return
        assert isinstance(identifier, str) and identifier != ""
        id_hash = sha256(identifier)
        if self.id_hash and id_hash != self.id_hash:
            return
        self._id_name = identifier
        self._set_id_hash(id_hash)
        self.is_named = True

    def _set_id_hash(self, id_hash: Union[bytes, str]):
        """
            Test if the hash given is a string or bytes
            Set the attribute identifier hash

        Parameters
        ----------
        id_hash: Union[bytes, str]
            The identifier hash to be set given as string or bytes
        """
        if self.is_named and to_bytes(id_hash) != sha256(self.id_name) or self._id_hash or not id_hash:
            return
        # When identifier hash is string the length must be 64 characters
        # When identifier hash is bytes the length must be 32 bytes
        assert (isinstance(id_hash, str) and len(id_hash) == 64) or \
               (isinstance(id_hash, bytes) and len(id_hash) == 32)
        self._id_hash = id_hash

    def _set_appropriated_address(self, appropriated_address: str):
        """
            Test if the address given is valid
            Set the attribute appropriated address

        Parameters
        ----------
        appropriated_address: str
            The appropriated address to be set
        """
        if not appropriated_address or self.appropriated_address:
            return
        assert is_address(appropriated_address)
        self.appropriated_address = appropriated_address

    def check_id_operation(self):
        """
            Check the conditions to match operation of this Identifier
        """
        # Sort oldest to the earliest block and remove duplicates with list(dict.fromkeys(x))
        block_heights = sorted(list(dict.fromkeys([each_tx['height'] for each_tx in self.transactions.values()])))
        for oldest_block in block_heights:
            appropriated_address = ''
            owner = None
            new_owner = None
            operation_txid = ''
            has_block_operation = False
            has_transferring = False
            for block_tx in [each_tx for each_tx in self.transactions.values()
                             if each_tx['height'] == oldest_block]:
                # Condition: the operation script is in the op return script
                has_id_operation, has_transferring, op_return_script = self.check_condition_script(
                    block_tx, self.is_owned, self.id_hash)
                if not has_id_operation:
                    # Speeds up the loop
                    continue

                # Condition: improper address is in output
                output_addresses = self.get_output_addresses(block_tx['output'])
                has_improper_address, improper_address, _ = self.check_condition_output(
                    output_addresses, op_return_script=op_return_script, improper_addresses=self.improper_addresses)
                if not has_improper_address:
                    # Speeds up the loop
                    continue
                # When the appropriated address is given, check against the improper addresses calculated
                elif self.appropriated_address and self.appropriated_address != improper_address:
                    continue
                # When the appropriated address is given, set the id_hash if the script has hash length
                elif self.appropriated_address and len(op_return_script) == self.APPROPRIATION_LENGTH:
                    self._set_id_hash(op_return_script[self.OP_MESSAGE:])

                # Condition: has an address in output to the Identifier be transferred
                has_transferred_addr, transferred_address = self.check_condition_transfer(
                    has_transferring, output_addresses, self.improper_addresses)
                if has_transferring != has_transferred_addr:
                    # Speeds up the loop
                    continue
                elif has_transferring:
                    new_owner = OwnerAddress(self.parent, owner_address=transferred_address)

                # Condition: a valid owner appropriated the identifier
                input_address = list(self.get_input_address(block_tx['txid']))
                # When a possible owner already evaluated is given do not re-evaluate it
                potential_owner = OwnerAddress(self.parent, owner_address=input_address[0]) \
                    if self.current_owner is None else self.current_owner
                has_owner_in_input = potential_owner.has_appropriated
                if not has_owner_in_input:
                    # Speeds up the loop
                    continue

                # Condition: There is a single operation in the mined block for this identifier
                if not has_block_operation:
                    has_block_operation = True
                    appropriated_address = improper_address
                    owner = new_owner if has_transferring else potential_owner if has_block_operation else None
                    operation_txid = block_tx['txid']
                else:
                    # When there is multiple operations in the mined block break the loop to the next block
                    # Else finish the loop and set the identifier ownership
                    break
            else:
                # When the loop is finished set the identifier ownership
                if has_block_operation:
                    self.set_identifier_ownership(has_transferring, operation_txid,
                                                  appropriated_address, owner)

    def set_identifier_ownership(self, has_transferring: bool,
                                 operation_txid: str,
                                 appropriated_address: str,
                                 owner: Union[str, 'OwnerAddress']):
        """
            Test the given addresses and set the private key correspondent to the appropriated address

        Parameters
        ----------
        has_transferring: bool
            The evaluation if the transaction is an identifier transferring to a new owner
        operation_txid : str
            The transaction ID of the operation, that being appropriation of transferring of this Identifier
        appropriated_address : str
            The address that was appropriated to set the correspondent private key
        owner : Union[str, 'OwnerAddress']
            The current owner of this Identifier
        """
        if appropriated_address not in self.addr_key_pair.keys():
            return
        if not self.is_owned:
            self.id_privkey = self.addr_key_pair[appropriated_address]
            self._set_appropriated_address(appropriated_address)
            self.appropriation_txid = operation_txid
            self.is_owned = True
        self.current_operation_tx = self.transactions[operation_txid]
        self.current_owner = owner
        self.historic_operations_txids.append(operation_txid)
        self.historic_owner_addresses.append(owner.owner_address)


class OwnerAddress(IdealTransaction):
    def __init__(self, parent: 'ElectrumWindow', *, owner_address: str):
        super().__init__(parent)
        assert is_address(owner_address)
        self.owner_address = owner_address
        self.has_appropriated = False
        self.has_transferred = False
        self.appropriation_txid = ''
        self.current_operation_tx = OrderedDictWithIndex()
        self.appropriated_hash = ''
        self.transferred_to = ''
        self.transfer_txid = ''

        self.check_id_operation()

    def __str__(self) -> str:
        """
            Customize the string representation of this class instance

        Returns
        -------
        str
            The string representation of this class instance with the main information
            followed by the address appropriated if is the case
        """
        header = ["Address"]
        attributes = [self.owner_address]
        if self.has_appropriated or self.has_transferred:
            header += ["Address Appropriated"]
            attributes += [self.appropriated_address]
        string_representation = ", ".join([" ".join(each_pair) for each_pair in zip(header, attributes)])
        return string_representation

    @property
    def transactions(self) -> OrderedDictWithIndex:
        """
            Transactions related to this address

        Returns
        -------
        OrderedDictWithIndex
            Returns the transactions filtered with OP_Returns
        """
        # This condition is applied to shorten the time of transactions by not updating every access to it
        if not self._transactions and self.parent.network.is_connected():
            txids = self.ask_txids([self.owner_address])
            transactions = self.load_transactions(txids)
            self._transactions = self.filter_op_returns(transactions)
        return self._transactions

    def check_id_operation(self):
        """
            Check the conditions to match Identifier operation by this Owner
        """
        # Sort oldest to the earliest block and remove duplicates with list(dict.fromkeys(x))
        block_heights = sorted(list(dict.fromkeys([each_tx['height'] for each_tx in self.transactions.values()])))
        for oldest_block in block_heights:
            appropriated_address = ''
            owner_address = ''
            operation_txid = ''
            has_block_operation = False
            has_transferring = False
            for block_tx in [each_tx for each_tx in self.transactions.values()
                             if each_tx['height'] == oldest_block]:

                # Condition: The operation script is in the op return script
                has_id_operation, has_transferring, op_return_script = self.check_condition_script(
                    block_tx, self.has_appropriated)
                if not has_id_operation:
                    # Speeds up the loop
                    continue

                # Condition: A single improper address is in output
                output_addresses = self.get_output_addresses(block_tx['output'])
                has_improper_address, improper_address, improper_addresses = self.check_condition_output(
                    output_addresses, op_return_script=op_return_script, owner_address=self.owner_address)
                if not has_improper_address:
                    # Speeds up the loop
                    continue

                # Condition: The transaction has an address in output to the Identifier be transferred
                has_transferred_addr, transferred_address = self.check_condition_transfer(
                    has_transferring, output_addresses, improper_addresses)
                if has_transferring != has_transferred_addr:
                    # Speeds up the loop
                    continue

                # Condition: This owner is in input or received transferring
                input_address = list(self.get_input_address(block_tx['txid']))
                has_owner_in_input = not has_transferring and self.owner_address in input_address
                has_owner_in_output = has_transferring and self.owner_address in output_addresses
                has_owner_in_operation = (has_owner_in_input or has_owner_in_output) and len(input_address) == 1
                if has_owner_in_input:
                    input_address = input_address[0]
                elif not has_owner_in_operation:
                    # Speeds up the loop
                    continue

                # Condition: There is a single operation in the mined block for this owner
                if not has_block_operation:
                    has_block_operation = True
                    appropriated_address = improper_address
                    owner_address = input_address if not transferred_address else transferred_address
                    operation_txid = block_tx['txid']
                else:
                    # When there is multiple operations in the mined block break the loop to the next block
                    # Else finish the loop and set the identifier ownership
                    break
            else:
                # When the loop is finished set the identifier ownership
                if has_block_operation:
                    self.set_identifier_ownership(has_transferring, operation_txid,
                                                  appropriated_address, owner_address)

    def set_identifier_ownership(self, has_transferring: bool, operation_txid: str,
                                 appropriated_address: str, owner_address: str):
        """
            Test the given addresses and set the possible owner to the appropriated or transferred address

        Parameters
        ----------
        has_transferring: bool
            The evaluation if the transaction is an identifier transferring to a new owner
        operation_txid : str
            The transaction ID of the operation, that being appropriation or transferring of this Identifier
        appropriated_address : str
            The address that was appropriated to set the correspondent private key
        owner_address : str
            The address of current owner of this Identifier
        """
        if has_transferring:
            self.transferred_to = owner_address
            self.appropriated_address = appropriated_address
            self.transfer_txid = operation_txid
            self.has_transferred = True
        elif not self.has_appropriated:
            self.appropriated_address = appropriated_address
            self.appropriation_txid = operation_txid
            self.has_appropriated = True
        self.current_operation_tx = self.transactions[operation_txid]


class IdealList(MyTreeView):
    """
        Build ideal tab

    Parameters
    ----------
    MyTreeView() : Class at util.py
    """

    class Columns(IntEnum):
        """
            Columns names for ideal table

        Parameters
        ----------
        IntEnum() : IntEnum
        """
        STATUS = 0
        DATE = 1
        IDENTIFIER = 2
        OWNER = 3
        HASH = 4

    headers = {
        Columns.STATUS: _('Status'),
        Columns.DATE: "Date",
        Columns.IDENTIFIER: "ID",
        Columns.OWNER: "Owner Address",
        Columns.HASH: "Appropriation Hash",
    }

    filter_columns = [Columns.STATUS, Columns.DATE, Columns.OWNER]
    # The arbitrary maximum characters shown in ID column
    NAME_LEN = 25

    def __init__(self, parent):
        super().__init__(parent, self.create_menu,
                         stretch_column=self.Columns.HASH)
        self.standard_model = QStandardItemModel(self)
        self.proxy = MySortModel(self, sort_role=ROLE_SORT_ORDER)
        self.proxy.setSourceModel(self.standard_model)
        self.setModel(self.proxy)
        self.setSortingEnabled(True)
        self.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.identifier_info = list()
        self.updated_hashes = {}
        self.invoice = list()
        self.update()

    def update(self):
        """
            Updates owned identifier table with hashes of updated identifiers
        """
        updated_hashes = self.parent.ideal_model.followed_identifiers
        updated_hashes.update(self.parent.ideal_model.owned_hashes)

        if updated_hashes:
            # Updates a conflicting identifier with new information and keep its last status
            for each_value in updated_hashes.values():
                id_hash = each_value['id_hash'].hex()
                if id_hash in self.updated_hashes.keys():
                    if each_value['status'] == "Followed" and \
                            each_value['status'] != self.updated_hashes[id_hash]['status']:
                        status = self.updated_hashes[id_hash]['status']
                        self.updated_hashes.update({id_hash: each_value})
                        self.updated_hashes[id_hash]['status'] = status
                else:
                    self.updated_hashes.update({each_value['id_hash'].hex(): each_value})

        # DO NOT REMOVE list(): PYTHON BUG
        self.identifier_info = list(enumerate(self.updated_hashes.items()))
        # not calling maybe_defer_update() as it interferes with conditional-visibility
        # temp. disable re-sorting after every change
        self.proxy.setDynamicSortFilter(False)
        self.standard_model.clear()
        self.update_headers(self.__class__.headers)

        for index, item in self.identifier_info:
            key = item[0]
            icon_name = 'bitcoin.png'
            status = 3
            # Information gathered to fill identifier table
            status_str = item[1]['status']
            identifier_hash = item[1]['id_hash'].hex()
            timestamp = item[1]['timestamp']
            date_str = format_time(timestamp) if timestamp else _('Unknown')
            if item[1]["address"] in self.parent.ideal_model.followed_id_owners.keys():
                identifier = self.parent.ideal_model.followed_id_owners[item[1]['address']]['identifier']
            else:
                identifier = _('Unknown')
            if len(identifier) > self.NAME_LEN:
                identifier = identifier[:self.NAME_LEN-3] + "..."
            owner_address = item[1]['address']
            labels = [status_str, date_str, identifier, owner_address, identifier_hash]
            items = [QStandardItem(e) for e in labels]
            self.set_editability(items)
            # Insert icons in table
            items[self.Columns.DATE].setIcon(read_QIcon(icon_name))
            items[self.Columns.STATUS].setIcon(read_QIcon(pr_icons.get(status)))
            items[self.Columns.DATE].setData(key, role=ROLE_REQUEST_ID)
            # Insert row in table
            self.standard_model.insertRow(index, items)
        self.filter()
        self.proxy.setDynamicSortFilter(True)
        # sort requests by date
        self.sortByColumn(self.Columns.DATE, Qt.DescendingOrder)
        # hide list if empty
        if self.parent.isVisible():
            has_rows = self.standard_model.rowCount() > 0
            self.setVisible(has_rows)
            self.parent.invoices_label.setVisible(has_rows)

    def create_menu(self, position: QPoint):
        """
            Method called when right-click item in the owned identifier table

        Parameters
        ----------
        position: QPoint
            Position in the table of clicked item
        """
        items = self.selected_in_column(0)
        # Case where multiple transactions are selected do nothing
        if len(items) > 1:
            return
        # Check which transaction is selected
        idx = self.indexAt(position)
        item = self.item_from_index(idx)
        item_col0 = self.item_from_index(idx.sibling(idx.row(), self.Columns.DATE))
        if not item or not item_col0:
            return
        key = item_col0.data(ROLE_REQUEST_ID)
        self.invoice = [item for index, item in self.identifier_info if item[0] == key]
        menu = QMenu(self)
        self.add_copy_menu(menu, idx)
        # Known bug partially solved: this operation can be use only once
        #  the variable behaviours as it being spent clearing itself
        #  this is caused by some incompatibility with python3 and PyQt5 using python2

        # Add operations to Identifier selected only if owned
        if self.invoice[0][1]['status'] == "Owned":
            # Functions associated with right click menu options
            menu.addAction("Transfer Identifier", lambda: self.parent.on_click_transfer())
            # Line commented: Future Pricing implementation
            # menu.addAction("Set Price to Identifier", lambda: self.parent.on_click_pricing())
        menu.exec_(self.viewport().mapToGlobal(position))

    def add_copy_menu(self, menu: QMenu, idx: QModelIndex) -> QMenu:
        """
            Create the menu options to copy information from the table
        Parameters
        ----------
        menu : QMenu
            The menu parent where the option copy appears
        idx : QModelIndex
            The index of object clicked in the table
        Returns
        -------
        QMenu
            It returns a new menu with the options to be copied
        """
        copy_action = menu.addMenu(_("Copy"))
        for column in self.Columns:
            column_title = self.original_model().horizontalHeaderItem(column).text()
            if not column_title:
                continue
            item_col = self.item_from_index(idx.sibling(idx.row(), column))
            clipboard_data = item_col.data(self.ROLE_CLIPBOARD_DATA)
            if clipboard_data is None:
                # If the Identifier is shortened gets the whole text
                clipboard_data = item_col.text().strip() if item_col.text().strip()[self.NAME_LEN-3:] != "..." else \
                    self.parent.ideal_model.followed_id_owners[self.invoice[0][1]['address']]['identifier']
            copy_action.addAction(column_title,
                                  lambda text=clipboard_data, title=column_title:
                                  self.place_text_on_clipboard(text, title=title))
        owner_address = self.item_from_index(idx.sibling(idx.row(), self.Columns.OWNER)).text().strip()
        clipboard_data = ""
        # If the transactions is from address of own wallet get the pubkey directly, else get it from the witness
        if self.parent.wallet.is_mine(owner_address):
            clipboard_data = self.parent.wallet.get_public_keys(owner_address)[0]
        elif self.invoice[0][1]['tx_object'] is not None:
            clipboard_data = self.invoice[0][1]['tx_object']['input'][0].witness.hex()[-66:]
        copy_action.addAction("Public key",
                              lambda text=clipboard_data, title="Public key":
                              self.place_text_on_clipboard(text, title="Public key"))
        improper_address = self.invoice[0][1]['improper_address']
        copy_action.addAction("Improper Address",
                              lambda text=improper_address, title="Improper Address":
                              self.place_text_on_clipboard(text, title="Improper Address"))
        return copy_action


class IdealModel(IdealTransaction):
    """
        Class to deal with wallet, addresses and transactions operations with Identifiers
    """
    def __init__(self, parent: 'ElectrumWindow'):
        super().__init__(parent)
        self.parent = parent
        self.owned_hashes = {}
        self.identifiers = []
        self.followed_identifiers = {}
        self.followed_id_owners = {}
        self.use_restricted_characters = True
        simbols = "%-.:=?_~&+/"
        self.alphanumcased_chars = sorted(string.ascii_letters + string.digits) + list(simbols + " ")
        self.lowernumeric_chars = sorted(string.ascii_lowercase + string.digits) + list(simbols)
        self.most_restricted = sorted(string.ascii_lowercase)
        self.restriction_options = [self.alphanumcased_chars, self.lowernumeric_chars, self.most_restricted]
        self.chosen_restriction_index = 0

    def check_characters(self, test_str: str) -> bool:
        """
            Check whether set of standard characters contains all the items in the string argument.
        Parameters
        ----------
        test_str : str
            The string to be tested

        Returns
        -------
        bool
            True if all characters given are standard, False if there is any non-standard character
        """
        test_chosen = self.restriction_options[self.chosen_restriction_index]
        # Method set().issubset() is faster than list comprehension
        remaining_chars = set(test_str).issubset(set(test_chosen))
        return remaining_chars

    def update_owned_hashes(self):
        """
            Read transactions of appropriation or transferring of Identifiers
        """
        # Getting transactions with Op_Returns and arranging as list by each used wallet address,
        #  ignoring unused addresses saves precious time
        wallet_addresses = [each_addr for each_addr in self.parent.wallet.get_addresses()
                            if each_addr not in self.parent.wallet.get_unused_addresses()]
        possible_owners = [OwnerAddress(self.parent, owner_address=each_address)
                           for each_address in wallet_addresses]
        possible_owners = [each_owner for each_owner in possible_owners
                           if each_owner.has_appropriated or each_owner.has_transferred]
        self.identifiers = [Identifier(self.parent, owner=each_owner,
                                       appropriated_address=each_owner.appropriated_address)
                            for each_owner in possible_owners]
        for each_id in self.identifiers:
            status = "Transferred" if each_id.current_owner.has_transferred else "Owned"
            timestamp = each_id.block_header['timestamp'] if each_id.block_header is not None else None
            self.owned_hashes.update(
                {each_id.id_hash: {
                    "id_hash": each_id.id_hash,
                    "address": each_id.current_owner.owner_address,
                    "improper_address": each_id.appropriated_address,
                    "tx_object": each_id.appropriation_transaction,
                    "id_object": each_id,
                    "status": status,
                    "timestamp": timestamp}})
        self.check_followed_identifiers()

    def update_followed_identifiers(self, *,
                                    identifier: 'Identifier' = None,
                                    identifier_string: str = '',
                                    id_hash: Union[str, bytes] = '',
                                    id_unknown: str = ""):
        """
            Updates the attribute self.followed_identifiers with Identifier information
        Parameters
        ----------
        identifier: 'Identifier'
            The identifier object which is being followed
        identifier_string: str
            The identifier name which is being followed
        id_hash: str
            The hash of identifier which is being followed
        id_unknown: str
            When the hash is given the status is named as a numbered unknown by id_unknown
        """
        if not identifier:
            # Identifier name or hash must be given exclusively, xor(x,y)
            if identifier_string and id_hash or not identifier_string and not id_hash:
                return
            if identifier_string:
                identifier = Identifier(self.parent, identifier=identifier_string)
            else:
                identifier = Identifier(self.parent, id_hash=id_hash)
                identifier_string = id_unknown

        if identifier.is_owned:
            status = "Followed"
            timestamp = identifier.block_header['timestamp'] if identifier.block_header is not None else None

            self.followed_identifiers.update(
                {identifier_string: {"address": identifier.current_owner.owner_address,
                                     "id_hash": identifier.id_hash,
                                     "improper_address": identifier.appropriated_address,
                                     "tx_object": identifier.current_operation_tx,
                                     "id_object": identifier,
                                     "status": status,
                                     "timestamp": timestamp}})
            self.followed_id_owners.update({identifier.current_owner.owner_address:
                                            {"identifier": identifier_string}})

    def check_followed_identifiers(self):
        """
            Check all contacts and update followed identifiers by each contact and known owned identifier hashes
        """
        # Update followed identifiers from contacts
        contacts = self.parent.wallet.contacts
        for each_contact in contacts.keys():
            identifier = contacts[each_contact][1]
            self.update_followed_identifiers(identifier_string=identifier)

        # Label unidentified owned hash as numbered unknown
        n = 1
        id_hashes = [sha256(each_contact[1]) for each_contact in contacts.values()]
        # Maximum leading zeros to fill the unknown identifiers
        expected = len(str(len(self.owned_hashes.keys())))
        for each_hash in self.owned_hashes.keys():
            # If the hash is identified skip to the next value
            if each_hash in id_hashes:
                continue
            unknown_id = "Unknown " + str(n).zfill(expected)
            n += 1
            self.update_followed_identifiers(id_hash=each_hash, id_unknown=unknown_id)


class AboutWindow:
    def show_window(self, title: str, window_text: str, image: str):
        """
            Opens the window with a text with some explanation and an image
            The image resizes according to the window size, with a minimum width of 64 pixels

        Parameters
        ----------
        title : str
            The title of the window
        window_text : str
            The text to be shown in the window
        image : str
            The file name of the image to be shown in the window
        """
        class ResizerWindow(QDialog):
            def resizeEvent(self, event):
                """
                    This method overrides resizeEvent of QDialog to scale the image according to the window size
                """
                if event.type() == QEvent.Resize:
                    # scale changes from 0.9 to 1 when the image width is about 64 pixels
                    scale = 0.9 if label_image.size().width() > 64 and label_image.size().height() > 64 else 1
                    label_image.setPixmap(about_image.scaled(
                        scale * label_image.size(), Qt.KeepAspectRatio))
                QMainWindow.resizeEvent(self, event)

        window_about = ResizerWindow()
        # Initializing labels and layout
        label_text = QLabel()
        label_image = QLabel()
        hbox = QHBoxLayout()
        # Setting text and image
        label_text.setText(window_text)
        about_image = QPixmap(icon_path(image))
        label_image.setPixmap(about_image)
        label_image.setAlignment(Qt.AlignTop)
        # Setting resizing event which will resize the image according to window size
        label_image.installEventFilter(window_about)
        # Adding widgets to the horizontal layout
        hbox.addWidget(label_image)
        hbox.addWidget(label_text)
        # Setting window layout and title
        window_about.setLayout(hbox)
        window_about.setWindowTitle(title)
        # Setting maximum size
        screen = QApplication.primaryScreen()
        screen_width = screen.size().width()
        screen_height = screen.size().height()
        window_about.setMaximumWidth(screen_width)
        window_about.setMaximumHeight(screen_height)
        # Opening the 'about' window
        window_about.exec()


async def do_asynchronous_function(do_function: Callable, args: Any = None):
    """
        Schedule a function or method to run asynchronously

    Parameters
    ----------
    do_function : Callable
        The given function to run
    args : Any
        The possible arguments of the given function
    """
    try:
        if args:
            await asyncio.gather(asyncio.to_thread(do_function, args))
        else:
            await asyncio.gather(asyncio.to_thread(do_function))
    except TimeoutError as e:
        print(e)
