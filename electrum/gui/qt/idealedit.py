#!/usr/bin/env python
#
# Electrum - lightweight Bitcoin client
# Copyright (C) 2012 thomasv@gitorious
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys
from typing import NamedTuple, TYPE_CHECKING
from PyQt5.QtGui import QFontMetrics, QFont, QTextCursor

from electrum.logging import Logger
from .completion_text_edit import CompletionTextEdit
from .util import MONOSPACE_FONT

if TYPE_CHECKING:
    from .main_window import ElectrumWindow

# Disabling the python cache creation found as __pycache__ files in the repository,
# this may affect python performance but don't mistake with the performance of network requisitions
sys.dont_write_bytecode = True

# Copy of paytoedit.py

RE_ALIAS = r"(.*?)\s*\<([0-9A-Za-z]{1,})\>"

frozen_style = "QWidget {border:none;}"
normal_style = "QPlainTextEdit { }"


class IdealEdit(CompletionTextEdit, Logger):
    """
        Class to deal with text input in textbox
    """
    def __init__(self, electrum_window: "ElectrumWindow"):
        CompletionTextEdit.__init__(self)
        Logger.__init__(self)
        self.electrum_window = electrum_window
        self.setFont(QFont(MONOSPACE_FONT))
        self.document().contentsChanged.connect(self.update_textbox_size)
        self.heightMin = 0
        self.heightMax = 150
        self.update_textbox_size()

    def set_frozen(self, freeze):
        self.setReadOnly(freeze)
        self.setStyleSheet(frozen_style if freeze else normal_style)
        for button in self.buttons:
            button.setHidden(freeze)

    def insert_completion(self, completion: str):
        """
            Insert the text in the autocomplete
            Overloaded method changing the string ending, the Identifier must be the exact string searched

        Parameters
        ----------
        completion : str
            The string to be filled in the autocomplete
        """
        if self.completer.widget() != self:
            return
        text_cursor = self.textCursor()
        extra = len(completion) - len(self.completer.completionPrefix())
        text_cursor.movePosition(QTextCursor.Left)
        text_cursor.movePosition(QTextCursor.EndOfWord)
        if extra == 0:
            text_cursor.insertText("")
        else:
            text_cursor.insertText(completion[-extra:])
        self.setTextCursor(text_cursor)

    def update_textbox_size(self):
        """Configure text box size"""
        line_height = QFontMetrics(self.document().defaultFont()).height()
        doc_height = self.document().size().height()
        min_possible_height = 11
        textbox_height = round(doc_height * line_height + min_possible_height)
        textbox_height = min(max(textbox_height, self.heightMin), self.heightMax)
        self.setMinimumHeight(textbox_height)
        self.setMaximumHeight(textbox_height)
        self.verticalScrollBar().hide()
