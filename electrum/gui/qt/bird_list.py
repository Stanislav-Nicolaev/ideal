#!/usr/bin/env python
#
# Electrum - lightweight Bitcoin client
# Copyright (C) 2015 Thomas Voegtlin
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import sys
from typing import TYPE_CHECKING, Union
from enum import IntEnum

from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5.QtCore import Qt, QPoint
from PyQt5.QtWidgets import QMenu, QAbstractItemView

from electrum.i18n import _
from electrum.util import format_time, OrderedDictWithIndex
from electrum.invoices import OnchainInvoice, PR_UNPAID, PR_FAILED

from .util import read_QIcon, MyTreeView, MySortModel, pr_icons
from .ideal_list import IdealTransaction, Identifier, OwnerAddress

if TYPE_CHECKING:
    from .main_window import ElectrumWindow

sys.dont_write_bytecode = True

ROLE_REQUEST_TYPE = Qt.UserRole
ROLE_REQUEST_ID = Qt.UserRole + 1
ROLE_SORT_ORDER = Qt.UserRole + 2


class BirdTransaction(IdealTransaction):

    # Script length in OP_Return which does not need op_code pushdata
    OP_CODE_SCRIPT_LEN = 78

    def __init__(self, parent: 'ElectrumWindow'):
        super().__init__(parent)

    def op_return_to_tweet(self, op_return_script: bytes) -> str:
        """
            Convert an OP_Return to a message correctly reading the bytes size

        Parameters
        ----------
        op_return_script : bytes
            The OP_Return bytes with the Bird message

        Returns
        -------
        str
            The Bird message posted in the OP_Return
        """
        message_begin = self.OP_MESSAGE \
            if len(op_return_script[self.OP_MESSAGE:]) < self.OP_CODE_SCRIPT_LEN \
            else self.OP_MESSAGE + 1
        try:
            message = op_return_script[message_begin:].decode(self.ENCODING)
        except UnicodeDecodeError:
            message = op_return_script[message_begin:].hex()
        return message


class HostAddress(BirdTransaction):
    def __init__(self, parent: 'ElectrumWindow',
                 *, owner: 'OwnerAddress',
                 host_address: str, is_mine: bool):
        super().__init__(parent)
        assert owner is not None
        assert host_address is not None
        assert is_mine is not None
        self.host_address = host_address
        self.is_mine = is_mine
        self.topic_owner = owner
        self.is_topic_host = False
        self.bird_messages = OrderedDictWithIndex()
        if host_address != owner.owner_address:
            self.check_id_operation()

    def __str__(self) -> str:
        """
            Customize the string representation of this class instance

        Returns
        -------
        str
            The string representation of this class instance with the main information
            followed by the messages if there is any
        """
        header = ["Host Address"+":", "Topic Owner"+":"]
        attributes = [self.host_address, self.topic_owner.owner_address]
        if self.bird_messages:
            for each_message in self.bird_messages.values():
                header += ["Message"+":"]
                outputs = each_message['output']
                op_return_script = [each_output.scriptpubkey for each_output in outputs
                                    if self.is_op_return(each_output.scriptpubkey)]
                op_return_script = op_return_script[0]
                message = self.op_return_to_tweet(op_return_script)
                attributes += [message]
        string_representation = ", ".join([" ".join(each_pair) for each_pair in zip(header, attributes)])

        return string_representation

    def check_id_operation(self):
        """
            Check the conditions to split Bird messages from Identifier operation
        """
        if not self.topic_owner.has_appropriated and not self.topic_owner.has_transferred:
            return
        # Sort oldest to the earliest block and remove duplicates with list(dict.fromkeys(x))
        block_heights = sorted(list(dict.fromkeys([each_tx['height']
                                                   for each_tx in self.topic_owner.transactions.values()])))
        for oldest_block in block_heights:
            for block_tx in [each_tx for each_tx in self.topic_owner.transactions.values()
                             if each_tx['height'] == oldest_block]:

                # Condition: The operation script is not in the op return script
                has_id_operation, has_transferring, op_return_script = self.check_condition_script(
                    block_tx, self.topic_owner.has_appropriated)
                if has_id_operation:
                    # Speeds up the loop
                    continue

                # Condition: This host address is in output and there is no improper address in output
                output_addresses = self.get_output_addresses(block_tx['output'])
                has_improper_address, improper_address, improper_addresses = self.check_condition_output(
                    output_addresses, op_return_script=op_return_script, owner_address=self.topic_owner.owner_address)
                if has_improper_address or self.host_address not in output_addresses:
                    # Speeds up the loop
                    continue

                # Condition: The owner of the identifier is in input
                input_address = list(self.get_input_address(block_tx['txid'], self.topic_owner.transactions))
                has_owner_in_input = self.topic_owner.owner_address in input_address
                has_owner_in_operation = has_owner_in_input and len(input_address) == 1
                if not has_owner_in_operation:
                    # Speeds up the loop
                    continue
                self.set_thread_hosting(block_tx)

    def set_thread_hosting(self, message_tx: Union[dict, OrderedDictWithIndex]):
        """
            Set the messages of this host with timestamp and topic_owner

        Parameters
        ----------
        message_tx : Union[dict, OrderedDictWithIndex]
            The dictionary with a message transaction to be set
        """
        self.bird_messages.update({message_tx['txid']: message_tx})
        block_header = self.ask_block_header(message_tx['height'])
        timestamp = block_header['timestamp'] \
            if block_header is not None else None
        self.bird_messages[message_tx['txid']].update({'timestamp': timestamp})
        self.bird_messages[message_tx['txid']].update({'topic_owner': self.topic_owner.owner_address})
        self.is_topic_host = True


class BirdList(BirdTransaction, MyTreeView):
    """
        Create table at bird tab

    Parameters
    ----------
    'MyTreeView' : QTreeView
    """

    class Columns(IntEnum):
        """
            Define Columns constants

        Parameters
        ----------
        'IntEnum' : enum.IntEnum
        """
        DATE = 0
        MESSAGE = 1
        IDENTIFICATION = 2
        STATUS = 3
        THREAD = 4

    headers = {
        Columns.DATE: 'Date or Block',
        Columns.MESSAGE: _('Message'),
        Columns.IDENTIFICATION: 'ID',
        Columns.STATUS: _('Status'),
        Columns.THREAD: 'Thread',
    }
    filter_columns = [Columns.DATE, Columns.IDENTIFICATION, Columns.THREAD]
    # The arbitrary maximum characters shown in ID column
    NAME_LEN = 25

    def __init__(self, parent):
        """
            Set the parameters to build the bird table structure

        Parameters
        ----------
        parent : ElectrumWindow
            The electrum main GUI
        """
        BirdTransaction.__init__(self, parent)
        MyTreeView.__init__(self, parent, self.create_menu)
        self.standard_model_bird = QStandardItemModel(self)
        # Sorting table
        self.proxy = MySortModel(self, sort_role=ROLE_SORT_ORDER)
        self.proxy.setSourceModel(self.standard_model_bird)
        self.setModel(self.proxy)
        self.setSortingEnabled(True)
        self.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.bird_messages = []
        self.update()

    def update(self):
        """
            Updates the bird table with all tweets
        """
        # not calling maybe_defer_update() as it interferes with conditional-visibility
        # temp. disable re-sorting after every change
        self.proxy.setDynamicSortFilter(False)
        # Clears table to not repeat items, saving them twice
        self.standard_model_bird.clear()
        self.update_headers(self.__class__.headers)
        t_idx = 0
        for idx, item in enumerate(self.bird_messages):

            if isinstance(item, OnchainInvoice):
                # Check if it is a tweet or normal transaction
                if item.bird is None:
                    continue
                items = self.update_type_invoice(item)
            elif isinstance(item, dict) or isinstance(item, OrderedDictWithIndex):
                items = self.update_type_transaction(item)
            else:
                continue

            # Saves new line
            self.standard_model_bird.insertRow(t_idx, items)
            t_idx += 1
        self.filter()
        self.proxy.setDynamicSortFilter(True)
        # sort requests by date
        self.sortByColumn(self.Columns.DATE, Qt.DescendingOrder)
        # hide list if empty
        if self.parent.isVisible():
            has_rows = self.standard_model_bird.rowCount() > 0
            self.setVisible(has_rows)
            self.parent.invoices_label.setVisible(has_rows)

    def update_type_invoice(self, item: OnchainInvoice) -> list[QStandardItem]:
        """
            Treat table element of type Invoice

        Parameters
        ----------
        item : OnchainInvoice
            The item to be inserted in the table

        Returns
        -------
        list[QStandardItem]
            The list with the values of the table columns ready to be inserted
        """
        key = self.parent.wallet.get_key_for_outgoing_invoice(item)
        if item.is_lightning():
            icon_name = 'lightning.png'
        else:
            icon_name = 'bitcoin.png'
            if item.bip70:
                icon_name = 'seal.png'
        # Get each element to be saved in table
        status = self.parent.wallet.get_invoice_status(item)
        message = item.message
        timestamp = item.time or 0
        date_str = format_time(timestamp) if timestamp else _('Unknown')
        posting_as = self.parent.ideal_model.followed_id_owners[item.posting_as]['identifier']
        if len(posting_as) > self.NAME_LEN:
            posting_as = posting_as[:self.NAME_LEN - 3] + "..."
        # Showing last characters of receiver address
        posting_at = item.outputs[0].address[-6:]
        status_str = item.get_status_str(status) + " " + item.bird
        labels = [date_str, message, posting_as, status_str, posting_at]
        items = [QStandardItem(e) for e in labels]
        self.set_editability(items)
        # Icon images
        items[self.Columns.DATE].setIcon(read_QIcon(icon_name))
        items[self.Columns.STATUS].setIcon(read_QIcon(pr_icons.get(status)))
        items[self.Columns.DATE].setData(key, role=ROLE_REQUEST_ID)
        items[self.Columns.DATE].setData(item.type, role=ROLE_REQUEST_TYPE)
        items[self.Columns.DATE].setData(timestamp, role=ROLE_SORT_ORDER)
        return items

    def update_type_transaction(self, item: Union[OrderedDictWithIndex, dict]):
        """
            Treat table element of type dictionary

        Parameters
        ----------
        item : Union[OrderedDictWithIndex, dict]
            The item to be inserted in the table

        Returns
        -------
        list[QStandardItem]
            The list with the values of the table columns ready to be inserted
        """
        key = item['txid']
        icon_name = 'bitcoin.png'
        # Get each element to be saved in table
        op_return_output = None
        post_output = None
        input_address = self.parent.ideal_model.get_input_address(item['txid'], {item['txid']: item})[0]
        for each_output in item['output']:
            if self.is_op_return(each_output.scriptpubkey):
                op_return_output = each_output
            elif each_output.address != input_address:
                post_output = each_output
        # When the Opcode and message is greater than 77 bytes the opcode OP_Pushdata is inserted after OP_Return
        # this means this script will never have 78 bytes of length
        # https://en.bitcoin.it/wiki/Script
        message_begin = self.OP_MESSAGE if len(op_return_output.scriptpubkey[2:]) < self.OP_CODE_SCRIPT_LEN \
            else self.OP_MESSAGE+1
        try:
            message = op_return_output.scriptpubkey[message_begin:].decode(self.ENCODING)
        except UnicodeDecodeError:
            message = op_return_output.scriptpubkey[message_begin:].hex()
        timestamp = item['timestamp']
        date_str = format_time(timestamp) if timestamp else _('Unknown')
        try:
            posting_as = self.parent.ideal_model.followed_id_owners[input_address]['identifier']
        except KeyError:
            posting_as = "Unknown"
        if len(posting_as) > self.NAME_LEN:
            posting_as = posting_as[:self.NAME_LEN] + "..."
        # Showing last characters of receiver address
        posting_at = post_output.address[-6:]
        topic_owner = item['topic_owner']
        status = "Post" if input_address == topic_owner else "React"
        status_str = status
        labels = [date_str, message, posting_as, status_str, posting_at]
        items = [QStandardItem(e) for e in labels]
        self.set_editability(items)
        # Icon images
        items[self.Columns.DATE].setIcon(read_QIcon(icon_name))
        items[self.Columns.DATE].setData(key, role=ROLE_REQUEST_ID)
        items[self.Columns.DATE].setData(timestamp, role=ROLE_SORT_ORDER)
        return items

    def insert_table_items(self, items: Union[OnchainInvoice, OrderedDictWithIndex, dict]):
        """
            Inserts Bird messages in the variable which will be used to update the table
        Parameters
        ----------
        items : Union[OnchainInvoice, OrderedDictWithIndex, dict]
            The items to be inserted in the table, which can be Invoices for payment request or
            dictionaries with transactions
        """
        if not items:
            return
        if isinstance(items, dict) or isinstance(items, OrderedDictWithIndex):
            items = items.values()
        bird_txids = [each_tx.id if isinstance(each_tx, OnchainInvoice) else each_tx["txid"]
                      for each_tx in self.bird_messages]
        # Insert the message in Bird messages if it was not included yet
        for each_item in items:
            txid = each_item.id if isinstance(each_item, OnchainInvoice) else each_item["txid"]
            if txid not in bird_txids:
                self.bird_messages.append(each_item)
                bird_txids.append(txid)

    def remove_table_item(self, key: str):
        """
            Remove a selected item from the table
        Parameters
        ----------
        key : str
            The ID of the item
        """
        self.parent.delete_invoices([key])

    def create_menu(self, position: QPoint):
        """
            Create menu when right click is pressed in table

        Parameters
        ----------
        position : PyQt5.QtCore.QPoint
            Which element is being selected in the table
        """
        wallet = self.parent.wallet
        items = self.selected_in_column(0)
        # Select more than one item
        if len(items) > 1:
            return
        idx = self.indexAt(position)
        item = self.item_from_index(idx)
        item_col0 = self.item_from_index(idx.sibling(idx.row(), self.Columns.DATE))
        # No items selected
        if not item or not item_col0:
            return
        key = item_col0.data(ROLE_REQUEST_ID)
        invoice = self.parent.wallet.get_invoice(key)
        if invoice is None:
            return
        menu = QMenu(self)
        self.add_copy_menu(menu, idx)
        if invoice.is_lightning():
            menu.addAction(_("Details"), lambda: self.parent.show_lightning_invoice(invoice))
        else:
            # Select only one item
            # Functions to be called when 'copy address', 'details', 'etc..' are clicked
            if len(invoice.outputs) == 1:
                menu.addAction(_("Copy Address"),
                               lambda: self.parent.do_copy(invoice.get_address(), title='Bitcoin Address'))
            menu.addAction(_("Details"), lambda: self.parent.show_onchain_invoice_bird(invoice))
        status = wallet.get_invoice_status(invoice)
        if status == PR_UNPAID:
            menu.addAction(_("Pay") + "...", lambda: self.parent.do_pay_invoice_bird(invoice))
        if status == PR_FAILED:
            menu.addAction(_("Retry"), lambda: self.parent.do_pay_invoice_bird(invoice))
        menu.addAction(_("Delete"), lambda: self.remove_table_item(key))
        menu.exec_(self.viewport().mapToGlobal(position))


class BirdModel(BirdTransaction):
    def __init__(self, parent: 'ElectrumWindow'):
        super().__init__(parent)
        self.parent = parent
        self.messages_history = OrderedDictWithIndex()

    def get_host_addresses(self, owned_hashes: dict, followed_identifiers: dict) -> list['HostAddress']:
        """
            Gets the addresses which hosts the Bird messages sent and from given followed Identifiers

        Parameters
        ----------
        owned_hashes : dict
            The dictionary with information about the current owner of an Identifier
        followed_identifiers :
            The dictionary with information about the Identifiers relevant to the user

        Returns
        -------
        list['HostAddress']
            The list of host addresses from wallet and host addresses from followed Identifiers
        """
        mine_host_addresses = []
        followed_host_addresses = []
        if owned_hashes:
            for each_id in owned_hashes.values():
                owner = each_id['id_object'].current_owner
                owner_transactions = owner.transactions
                outputs = [each_output for each_tx in owner_transactions.values()
                           for each_output in each_tx['output']]
                # Getting output addresses when it is not equal the owner address and improper addresses
                # Remove duplicates with list(dict.fromkeys(x))
                output_addresses = list(dict.fromkeys([each_output.address for each_output in outputs
                                                       if each_output.address is not None and
                                                       each_output.address != owner.owner_address and
                                                       each_output.address not in
                                                       each_id['id_object'].improper_addresses]))
                host_addresses = [HostAddress(self.parent,
                                              owner=owner,
                                              host_address=each_address,
                                              is_mine=True)
                                  for each_address in output_addresses]
                if host_addresses:
                    mine_host_addresses += [each_host for each_host in host_addresses
                                            if each_host.is_topic_host]
        if followed_identifiers:
            for each_id in followed_identifiers.values():
                if each_id['id_hash'] in owned_hashes.keys():
                    continue
                owner = each_id['id_object'].current_owner
                owner_transactions = owner.transactions
                outputs = [each_output for each_tx in owner_transactions.values()
                           for each_output in each_tx['output']]
                # Getting output addresses when it is not equal the owner address and improper addresses
                # Remove duplicates with list(dict.fromkeys(x))
                output_addresses = list(dict.fromkeys([each_output.address for each_output in outputs
                                                       if each_output.address is not None and
                                                       each_output.address != owner.owner_address and
                                                       each_output.address not in
                                                       each_id['id_object'].improper_addresses]))
                host_addresses = [HostAddress(self.parent,
                                              owner=owner,
                                              host_address=each_address,
                                              is_mine=False)
                                  for each_address in output_addresses]
                if host_addresses:
                    followed_host_addresses += [each_host for each_host in host_addresses
                                                if each_host.is_topic_host]
        # Remove duplicates with list(dict.fromkeys(x))
        host_addresses = list(dict.fromkeys(mine_host_addresses + followed_host_addresses))
        return host_addresses

    def update_messages(self, owned_hashes: dict,
                        followed_identifiers: dict) -> list[dict]:
        """
            Read OP_Returns with Bird messages

        Parameters
        ----------
        owned_hashes: dict
            The dictionary with the identifiers hashes owned by the addresses of the wallet
        followed_identifiers: dict
            The dictionary with the identifiers followed in the contacts of the wallet

        Returns
        -------
        list[dict]
            The transactions with Bird messages ready to be inserted in the Bird messages table
        """
        host_addresses = list(self.get_host_addresses(owned_hashes, followed_identifiers))
        tx_history = [each_host.bird_messages for each_host in host_addresses]
        self.update_messages_history(tx_history)
        return tx_history

    def update_messages_history(self, followed_history_tx: list[Union[dict, OrderedDictWithIndex]]):
        """
            Updates the messages when a new message is given

        Parameters
        ----------
        list[Union[dict, OrderedDictWithIndex]]
            The history of transactions with messages
        """
        for each_history in followed_history_tx:
            # List difference with list(set(x)-set(y))
            difference = list(set(each_history.keys()) & set(self.messages_history.keys()))
            [self.messages_history.update({each_history[each_diff]['txid']: each_history[each_diff]})
             for each_diff in difference]

    def is_host_address(self, host_address) -> bool:
        """
            Verify if it is a bird post by finding current Identifier owner

        Returns
        -------
        bool
            Returns True if the input address of OP_Return has an Identifier
        """
        txids = self.ask_txids([host_address])
        transactions = self.load_transactions(txids)
        op_return_tx = self.filter_op_returns(transactions)
        if not op_return_tx:
            return False
        possible_owner_addrs = [each_addr for each_tx in op_return_tx.values()
                                for each_addr in self.get_input_address(each_tx['txid'], op_return_tx)]
        possible_owners = [OwnerAddress(self.parent, owner_address=each_address)
                           for each_address in possible_owner_addrs]
        possible_owners = [each_owner for each_owner in possible_owners
                           if each_owner.has_appropriated or each_owner.has_transferred]
        identifiers = [Identifier(self.parent, owner=each_owner,
                                  appropriated_address=each_owner.appropriated_address)
                       for each_owner in possible_owners]
        identifiers = [each_id for each_id in identifiers if each_id.is_owned]
        return len(identifiers) == 1
