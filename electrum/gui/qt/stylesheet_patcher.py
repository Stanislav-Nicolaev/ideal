"""This is used to patch the QApplication style sheet.
It reads the current stylesheet, appends our modifications and sets the new stylesheet.
"""

import sys

from PyQt5 import QtWidgets
from electrum.gui.qt import PyQt5_stylesheets

CUSTOM_PATCH_FOR_DEFAULT_THEME_MACOS = '''
/* On macOS, main window status bar icons have ugly frame (see #6300) */
StatusBarButton {
    background-color: transparent;
    border: 1px solid transparent;
    border-radius: 4px;
    margin: 0px;
    padding: 2px;
}
StatusBarButton:checked {
  background-color: transparent;
  border: 1px solid #1464A0;
}
StatusBarButton:checked:disabled {
  border: 1px solid #14506E;
}
StatusBarButton:pressed {
  margin: 1px;
  background-color: transparent;
  border: 1px solid #1464A0;
}
StatusBarButton:disabled {
  border: none;
}
StatusBarButton:hover {
  border: 1px solid #148CD2;
}
'''


def patch_qt_stylesheet(use_dark_theme: str) -> None:
    custom_patch = ""
    app = QtWidgets.QApplication.instance()
    # if default light mode
    if use_dark_theme == 'default':
      # if darwin add custom settings
      if sys.platform == 'darwin':
          custom_patch = CUSTOM_PATCH_FOR_DEFAULT_THEME_MACOS
      style_sheet = app.styleSheet() + custom_patch
      app.setStyleSheet(style_sheet)
    # if any other layout is set
    else:
      app.setStyleSheet(PyQt5_stylesheets.load_stylesheet_pyqt5(style=use_dark_theme))
